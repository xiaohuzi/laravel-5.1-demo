@extends('layouts.master')

@section('title', 'Blade 视图')

@section('sidebar')
	@parent
	重写layout侧边栏数据
@endsection

{{-- @section('content', '视图内容') --}}

@section('content')

Name: {{$name}}
<br>
Type: {{$type}}

<br>
{{ $name1 or 'Default' }}
<br>
{!!$html !!}
<br>
{{$project}}

<br>

@if (count($records) === 1)
    I have one record!
@elseif (count($records) > 1)
    I have multiple records!
@else
    I don't have any records!
@endif

{{--
@for ($i = 0; $i < 10; $i++)
    The current value is {{ $i }}
@endfor

@foreach ($users as $user)
    <p>This is user {{ $user->id }}</p>
@endforeach

@while (true)
    <p>I'm looping forever.</p>
@endwhile

--}}


@endsection