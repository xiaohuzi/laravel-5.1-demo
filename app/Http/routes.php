<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 基本使用
*/

#匿名函数处理
Route::get('/', function () {
    return view('welcome');
});

#指定特定的控制器 action 使用Foo 控制器 create action 处理请求
Route::get('foo/create', 'Foo@create');
/** 
  路由参数
*/

#路由参数
Route::get('routeparam/{id}/{name}', function ($id, $name) {
	return 'ID: ' . $id . '<br>'. 'Name: ' . $name;
});

#可选参数
Route::get('routeparam/{name?}', function ($name = null) {
    return 'what is your name? ' . $name;
});


/**
 group 的使用
*/

#group中使用
#文档 http://laravel-china.org/docs/5.1/routing#route-groups
Route::group(['middleware' => 'MyMiddleware'], function () {
	Route::get('/', function () {
    	return view('welcome');
	});

	Route::get('api/update', 'Api\Update@index');
});

#路由命名空间
#请求url http://laravel.local/show/12 
Route::group(['namespace' => 'Admin'], function()
{
    // Controllers Within The "App\Http\Controllers\Admin" Namespace

    Route::group(['namespace' => 'User'], function()
    {
        // Controllers Within The "App\Http\Controllers\Admin\User" Namespace
        Route::get('show/{id}', 'ProfileController@show');
    });
});

#路由前缀
Route::group(['prefix' => 'admin'], function () {
    Route::get('users', function ()    {
        // Matches The "/admin/users" URL
        echo '路由前缀';
    });
});

/** 
中间件 使用
*/
#单项使用
Route::get('middleware', ['middleware' => 'MyMiddleware', function () {

}]);

#中间配合具体控制器使用
# 文档 - http://laravel-china.org/docs/5.1/controllers#controller-middleware
Route::get('foo/create', ['middleware' => 'MyMiddleware', 'uses' => 'Foo@create']);

#中间件传递参数
#文档 http://laravel-china.org/docs/5.1/middleware#middleware-parameters
#中间件 - role, 参数 - editor
Route::get('auth/{id}', ['middleware' => 'role:editor', function ($id) {
	echo 'temp code';
}]);


/**
	隐式控制器 - 一个控制器只需要配置一行路由规则
	文档 - http://laravel-china.org/docs/5.1/controllers#implicit-controllers
*/
Route::controller('myuser', 'MyUserController');
Route::controller('mysql', 'MysqlController');
Route::controller('eloquent', 'EloquentController');
/** 路由命名 */
//route('newfoo'); 、 action('Foo@index'); 输出 http://laravel.local/foo
Route::get('foo', ['uses' => 'Foo@index', 'as' => 'newfoo']);


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');


Route::get('foo/store', 'Foo@store');

Route::get('view', 'View@index');
Route::get('view/with', 'View@with');
Route::get('view/blade', 'View@blade');
// Route::get('user/{id}', 'UserController@showProfile');

// Route::get('admin/panel', 'Admin\Panel\PanelController@index');

// Route::get('api/sys/update', function () {
//     echo 'route 测试';
// });

// Route::any('foo', function () {
//     echo 'any 路由测试';
// });

// $router->get("/awesome/sauce", "AwesomeController@sauce", ['middleware' => 'auth']);
// Route::get('admin/panel', 'Admin\Panel\PanelController@index', ['middleware' => 'auth']);



// Route::group(['namespace' => 'Admin\Panel'], function () {
// 	Route::get('panel', ['as' => 'panel', 'uses' => 'PanelController@index']);
// });