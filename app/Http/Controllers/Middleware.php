<?php
/** 
中间件 在路由中的使用

#单项使用
Route::get('middleware', ['middleware' => 'MyMiddleware', function () {

}]);

#中间配合具体控制器使用
Route::get('foo/create', ['middleware' => 'MyMiddleware', 'uses' => 'Foo@create']);

#group中使用
Route::group(['middleware' => 'MyMiddleware'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('api/update', 'Api\Update@index');
});

#中间件传递参数
Route::get('auth/{id}', ['middleware' => 'role:editor', function ($id) {
    echo 'temp code';
}]);
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Middleware extends Controller
{

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //当前控制器的所有action应用 auth 中间件
        $this->middleware('auth');
        //只对当前控制器 fooAction、barAction 应用 log 中间件
        $this->middleware('log', ['only' => ['fooAction', 'barAction']]);
        //只对当前控制器除 fooAction、barAction 应用 subscribed 中间件
        $this->middleware('subscribed', ['except' => ['fooAction', 'barAction']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        echo 'Middleware 控制器 index Action';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
