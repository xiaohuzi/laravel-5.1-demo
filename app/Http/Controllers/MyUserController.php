<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MyUserController extends Controller
{
    /**
     * 入口url http://laravel.local/myuser 请求方法 get
     *
     * @return Response
     */
    public function getIndex()
    {
        echo 'get index';
    }

    /**
     * 入口url http://laravel.local/myuser/user-self-profile 请求方法 get
     *
     * @return Response
     */
    public function getUserSelfProfile()
    {
        echo 'This is Controller:MyUserController', PHP_EOL, 'Action:MyUserController';
    }

    /**
     * 接收url参数
     * 入口url http://laravel.local/myuser/params/12/php 请求方法 get
     */
    public function getParams($id, $name)
    {
        var_dump($id, $name);
    }
    
    /**
     * 入口url http://laravel.local/myuser/store 请求方法 post
     *
     * @return Response
     */
    public function postStore()
    {
        //
    }
   
}