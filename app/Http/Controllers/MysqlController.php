<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class MysqlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $res = DB::select('select * from story where id > ?', [1]);

        foreach ($res as $val) {
            echo $val->id, ' - ', $val->title, '<br>';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getUpdate()
    {
        $res = DB::update('update story set title=\'jsp\' where title = :title and id > :id', ['title' => 'java', 'id' => 2]);

        var_dump($res);
    }

    /**
     * 使用多个数据库连接
     *
     * @return Response
     */
    public function getMultipleConnect()
    {
        $res = DB::connection('mysql1')->select('select * from tb_post');

        foreach ($res as $val) {
            echo $val->id, ' - ', $val->title, '<br>';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
