<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class View extends Controller
{
    /**
     * 基本的视图显示、赋值
     *
     * @return Response
     */
    public function index()
    {
        $data = ['name' => 'php', 'type' => 'web'];
        return view('view.index', $data);
    }

   
    /**
     * blade视图模版使用
     *
     * @return Response
     */
    public function blade()
    {
        $data = [
            'name' => 'php', 
            'type' => 'web', 
            'html' => '<font color="red">HTML</font>',
            'records' => [1, 2, 3],
        ];
        return view('view.blade', $data);
    }

    /**
     * 使用with()进行单独赋值
     */
    public function with()
    {
        $view = view('view.with');
        $view->with('name', 'php');
        $view->with('type', 'web');

        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
