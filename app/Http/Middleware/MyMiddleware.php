<?php
/**
To create a new middleware, use the make:middleware Artisan command:

php artisan make:middleware OldMiddleware
*/

namespace App\Http\Middleware;

use Closure;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
        请求分发之前执行的动作 
         */
        echo '请求分发之前执行的动作', '<br>';
        return $next($request);

        /**
        请求分发之后执行的动作
        */
        $response = $next($request);
        echo '请求分发之后执行的动作';
        return $response;
    }
}
