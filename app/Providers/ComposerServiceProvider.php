<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        // view()->composer(
        //     'view.index', 'App\Http\ViewComposers\ProfileComposer'
        // );

        // Using Closure based composers...
        view()->composer('view', function ($view) {
                echo 'ddddddd';
        });

        // view()->composer('*', function ($view) {
        // echo 'dddddddddddd';
        // });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
